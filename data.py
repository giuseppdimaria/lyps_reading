import os
import cv2
from typing import List

import tensorflow as tf

import warnings
warnings.filterwarnings("ignore")




vocab = [x for x in "abcdefghijklmnopqrstuvwxyz'?!123456789 "]

char_to_num = tf.keras.layers.StringLookup(vocabulary=vocab, oov_token="")
num_to_char = tf.keras.layers.StringLookup(vocabulary=char_to_num.get_vocabulary(), oov_token="", invert=True)

def load_video(path: str) -> List[float]:
    cap = cv2.VideoCapture(path)
    frames = []
    for _ in range(int(cap.get(cv2.CAP_PROP_FRAME_COUNT))):
        ret, frame = cap.read()
        frame = tf.image.rgb_to_grayscale(frame)
        # Isolo l'area della bocca nei video del dataset (sono coord. delle posizioni)
        frames.append(frame[190:236, 80:220, :])
    cap.release()

    mean = tf.math.reduce_mean(frames)
    std = tf.math.reduce_std(tf.cast(frames, tf.float32))
    return tf.cast((frames - mean), tf.float32) / std




def load_alignments(path:str) -> List[str]:
    with open(path, 'r') as f:
        lines = f.readlines()
    tokens = []
    for line in lines:
        line = line.split()
        if line[2] != 'sil':
            tokens = [*tokens, ' ', line[2]]
    return char_to_num(tf.reshape(tf.strings.unicode_split(tokens, input_encoding='UTF-8'), (-1)))[1:]

def load_data(path: str):
    path = bytes.decode(path.numpy())
    # file_name = path.split('/')[-1].split('.')[0]
    # File name splitting for windows
    file_name = path.split('\\')[-1].split('.')[0]
    video_path = os.path.join('data', 's1', f'{file_name}.mpg')
    alignment_path = os.path.join('data', 'alignments', 's1', f'{file_name}.align')
    frames = load_video(video_path)
    alignments = load_alignments(alignment_path)

    return frames, alignments




def mappable_function(path:str) ->List[str]:
    result = tf.py_function(load_data, [path], (tf.float32, tf.int64))
    return result


def data_pipeline():
    data = tf.data.Dataset.list_files('./data/s1/*.mpg')
    data = data.shuffle(500, reshuffle_each_iteration=False)
    data = data.map(mappable_function)
    # i file alignments hanno numeri di caratteri diversi
    # 75 fotogrammi per ogni video, 40 token per ogni aligments
    data = data.padded_batch(2, padded_shapes=([75, None, None, None], [40]))
    #  costante che sceglie automaticamente il numero ottimale di elementi paralleli utilizzati per elaborare il dataset
    data = data.prefetch(tf.data.AUTOTUNE)

    return data




def train_test_split():
    data = data_pipeline()

    # Added for split
    train = data.take(450)
    test = data.skip(450)

    print("len train: ", len(train))
    print("len test: ", len(test))

    return train, test