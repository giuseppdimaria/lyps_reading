import os
import cv2
import dlib
import gdown
import imageio
import numpy as np
from typing import List
import tensorflow as tf
from matplotlib import pyplot as plt
from keras.callbacks import ModelCheckpoint, LearningRateScheduler

from data import vocab, char_to_num, num_to_char, load_video, load_alignments, load_data, mappable_function, \
    data_pipeline, train_test_split
from model import create_model, scheduler, CTCLoss, train_model
from prediction import prediction
from real_time_detection import real_time_capture



import warnings
warnings.filterwarnings("ignore")



physical_device = tf.config.list_physical_devices('GPU')
try:
    tf.config.experimental.set_memory_growth(physical_device[0], True)
except:
    pass
print(tf.config.list_physical_devices('GPU'))


def main():
    print("LIPS READING")
    model = create_model()
    # train_model(model)
    # prediction()
    real_time_capture(model)


if __name__ == '__main__':
    main()
