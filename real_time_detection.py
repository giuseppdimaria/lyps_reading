import cv2
import dlib
import tensorflow as tf

from data import num_to_char

import warnings
warnings.filterwarnings("ignore")






def real_time_capture(model):
    cap = cv2.VideoCapture(0)


    mouth_frames_cap = []

    face_detector = dlib.get_frontal_face_detector()
    face_landmark = dlib.shape_predictor('detector/shape_predictor_68_face_landmarks.dat')

    while cap.isOpened():
        ret, frame = cap.read()

        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  # delete color information

        faces = face_detector(frame)

        for k, d in enumerate(faces):

            shape = face_landmark(frame, d)

            xmouthpoints = [shape.part(x).x for x in range(48, 67)]
            ymouthpoints = [shape.part(x).y for x in range(48, 67)]
            maxx = max(xmouthpoints)
            minx = min(xmouthpoints)
            maxy = max(ymouthpoints)
            miny = min(ymouthpoints)
            pad = 10

            width = 140 - (maxx - minx)
            height = 46 - (maxy - miny)

            minx = minx - width // 2
            maxx = maxx + width // 2 + width % 2
            miny = miny - height // 2
            maxy = maxy + height // 2 + height % 2

            # drawing rectangle around lips on 'ret'
            cv2.rectangle(frame, (maxx, maxy), (minx, miny), (0, 0, 255), 1)
            # crop_image = frame[miny - pad:maxy + pad, minx - pad:maxx + pad]
            # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            crop_image = frame[miny:maxy, minx:maxx]
            crop_image = tf.image.rgb_to_grayscale(crop_image)

            mouth_frames_cap.append(crop_image)

        cv2.imshow('Real Time Lips Detection', frame)

        # frame = tf.image.rgb_to_grayscale(frame)
        # reshaped_frame = frame[190:236, 80:220, :]
        # frames_cap.append(reshaped_frame)


        """
            L'idea è quella di fare la predict di ciò che viene detto
                ogni 75 frames catturati dal video della vwebcam
        """
        if len(mouth_frames_cap) == 75:
            fp_0_exp = tf.expand_dims(mouth_frames_cap, axis=0)
            y_prediction = model.predict(fp_0_exp)
            decoded = tf.keras.backend.ctc_decode(y_prediction, input_length=[75], greedy=True)[0][0].numpy()
            print('Prediction:', tf.strings.reduce_join(num_to_char(decoded)).numpy().decode('utf-8'))
            mouth_frames_cap.clear()



        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


