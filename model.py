import os
import tensorflow as tf
from keras.optimizers import Adam
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.layers import Conv3D, LSTM, Dense, Dropout, Bidirectional, MaxPool3D, Activation, TimeDistributed, Flatten

from data import char_to_num, train_test_split

import warnings
warnings.filterwarnings("ignore")


def create_model():
    model = Sequential()
    model.add(Conv3D(128, 3, input_shape=(75, 46, 140, 1), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool3D((1, 2, 2)))

    model.add(Conv3D(256, 3, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool3D((1, 2, 2)))

    model.add(Conv3D(75, 3, padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPool3D((1, 2, 2)))

    model.add(TimeDistributed(Flatten()))

    model.add(Bidirectional(LSTM(128, kernel_initializer='Orthogonal', return_sequences=True)))
    model.add(Dropout(.5))

    model.add(Bidirectional(LSTM(128, kernel_initializer='Orthogonal', return_sequences=True)))
    model.add(Dropout(.5))

    model.add(Dense(char_to_num.vocabulary_size() + 1, kernel_initializer='he_normal', activation='softmax'))

    model.compile(optimizer=Adam(learning_rate=0.0001), loss=CTCLoss)

    summary = model.summary()
    print(summary)

    return model



def scheduler(epoch, lr):
    if epoch < 30:
        return lr
    else:
        return lr * tf.math.exp(-0.1)

def CTCLoss(y_true, y_pred):
    # calcola la lunghezza del batch (numeri di samples nel batch)
    batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
    # lunghezza delle sequenze generate dalla rete
    input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
    #  lunghezza delle sequenze target
    label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

    # crea tensori di lunghezza uniforme
    input_length = input_length * tf.ones(shape=(batch_len, 1), dtype="int64")
    label_length = label_length * tf.ones(shape=(batch_len, 1), dtype="int64")

    # converte la dimensione dei due input in interi, e li utilizza per calcolare la perdita con 'ctc_batch_cost'
    loss = tf.keras.backend.ctc_batch_cost(y_true, y_pred, input_length, label_length)

    return loss

# def load_weigths(path):
#     model = create_model()
#     model.load_weights(path)
#
#     return model



def train_model(model):
    train, test = train_test_split()

    model = create_model()

    checkpoint_callback = ModelCheckpoint(os.path.join('models', 'checkpoint'), monitor='loss', save_weights_only=True)
    schedule_callback = LearningRateScheduler(scheduler)

    model.fit(train, validation_data=test, epochs=30, callbacks=[checkpoint_callback, schedule_callback])
    model.save('cnn3D.h5')
