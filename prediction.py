import tensorflow as tf


from model import create_model
from data import num_to_char, load_data, data_pipeline

import warnings
warnings.filterwarnings("ignore")



def prediction():
    model = create_model()
    data = data_pipeline()


    frames, alignments = data.as_numpy_iterator().next()

    model.load_weights('models/checkpoint')




    """
    TEST ON VIDEO
    
    """
    sample = load_data(tf.convert_to_tensor('.\\data\\s1\\pgbr3a.mpg'))

    print('~'*100, 'REAL TEXT')
    print([tf.strings.reduce_join([num_to_char(word) for word in sentence]) for sentence in [sample[1]]])

    yhat = model.predict(tf.expand_dims(sample[0], axis=0))

    decoded = tf.keras.backend.ctc_decode(yhat, input_length=[75], greedy=True)[0][0].numpy()

    print('~'*100, 'PREDICTIONS')
    print([tf.strings.reduce_join([num_to_char(word) for word in sentence]) for sentence in decoded])
